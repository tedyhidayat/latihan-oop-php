<?php

// class Contohstatic {
// 	public static $angka = 1;

// 	public static function halo() {
// 		return "Halo. " . self::$angka++ . " Kali.";
// 	}
// }


// // cara manggil static pake ::

// echo Contohstatic::$angka;
// echo '<br>';
// echo Contohstatic::halo();

// echo '<hr>';

// echo Contohstatic::halo();


echo '<hr>';
echo 'Menrggunakan oop biasa';
echo '<hr>';

class Contoh {
	public $angka = 1;

	public function halo() {
		return "Halo " . $this->angka++ . " kali. <br>";
	}
}

$obj1 = new Contoh;
echo $obj1->halo();
echo $obj1->halo();
echo $obj1->halo();

echo '<hr>';

$obj2 = new Contoh;
echo $obj2->halo();
echo $obj2->halo();
echo $obj2->halo();

echo '<hr>';
echo 'Menrggunakan static';
echo '<hr>';



// menggunakan static methode
class Contoh2 {
	public static $angka = 1;

	public function halo() {
		return "Halo " . self::$angka++ . " kali. <br>";
	}
}

$obj1 = new Contoh2;
echo $obj1->halo();
echo $obj1->halo();
echo $obj1->halo();

echo '<hr>';

$obj2 = new Contoh2;
echo $obj2->halo();
echo $obj2->halo();
echo $obj2->halo();