<?php


class Produk {
	public $judul,
			$penulis,
			$penerbit,
			$harga,
			$jmlhalaman,
			$waktumain;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlhalaman = 0, $waktumain = 0 ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
		$this->jmlhalaman = $jmlhalaman;
		$this->waktumain = $waktumain;
	}

	public function getlabel() {
		return "$this->penulis, $this->penerbit";
	}

	public function getinfoproduk() {
		$str = "{$this->judul} | {$this->getlabel()} (Rp. {$this->harga})";

		return $str;
	}
}

class Novel extends Produk {
	public function getinfoproduk() {
		$str = "Novel : {$this->judul} | {$this->getlabel()} (Rp. {$this->harga}) - {$this->jmlhalaman} Halaman.";
		return $str;
	}
}
class Game extends Produk {
	public function getinfoproduk() {
		$str = "Game : {$this->judul} | {$this->getlabel()} (Rp. {$this->harga}) ~ {$this->waktumain} Jam.";
		return $str;
	}
}

class CetakInfoProduk {
	public function cetak ( Produk $produk ) {
		$str = "{$produk->judul} | {$produk->getlabel()} (Rp. {$produk->harga})";
		return $str;
	}
}


$produk1 = new Novel("Laskar Pelangi", "Andrea Hirata", "Media Kita", 50000, 100, 0);

$produk2 = new Game("Mortal Kombat", "Ubisoft", "Sony Game", 250000, 0, 30);



echo $produk1->getinfoproduk();
echo "<br>";
echo $produk2->getinfoproduk();


