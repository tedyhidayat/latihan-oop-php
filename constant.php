<?php

// konstanta usahakan menggunakan huruf kapital semua utk menbedakan dengan variable

// menggunakan define
// define tidak bisa dimasukan ke dalam class, berfungsi global
define('NAMA', 'Tedy Hidayat');
echo NAMA;

echo '<br>';

// menggunakan const
// const bisa dimasukan ke dalam class, berfungsi untuk oop
const UMUR = 19;
echo UMUR;

echo '<hr>';


class Coba {
	// define('NAMA', 'Tedy Hidayat'); Contoh error
	const NAMA = "Tedy Hidayat";
}

// cara memanggilnya seperti static keyword menggunakan ::
echo Coba::NAMA;


// php juga punya magic constant
/*
__LINE__
__FILE__
__DIR__
__FUNCTION__
__CLASS__
__TRAIT__
__METHOD__
__NAMESPACE__
*/

echo '<hr>';

echo __LINE__;
echo '<br>';

echo __FILE__;
echo '<br>';

echo __DIR__;
echo '<br>';


function Coba(){
	return __FUNCTION__;
}
echo Coba();
echo '<br>';

class Coba2 {
	public $class = __CLASS__;
}
$obj = new Coba2;
echo $obj->class;
echo '<br>';

class Coba3{
	public $angka = 1;

	public function tampil() {
		return __METHOD__;
	}
}

$obj2 = new Coba3;
echo $obj2->tampil();