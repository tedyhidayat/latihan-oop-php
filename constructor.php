<?php


class Produk{
	public $judul,
			$penulis,
			$penerbit,
			$harga;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0 ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getlabel() {
		return "$this->judul, $this->penulis";
	}
}


$produk1 = new Produk("Laskar Pelangi", "Andrea Hirata", "Media Kita", 50000);

$produk2 = new Produk("Belajar WEB Pemula", "Abdul Kadir", "Andi", 250000);

$produk3 = new Produk();


echo "Novel : " . $produk1->getlabel();
echo "<br>";
echo "Buku : " . $produk2->getlabel();
echo "<br>";
echo "Buku : " . $produk3->getlabel();
