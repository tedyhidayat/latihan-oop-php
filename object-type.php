<?php


class Produk {
	public $judul,
			$penulis,
			$penerbit,
			$harga;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0 ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getlabel() {
		return "$this->penulis, $this->penerbit";
	}
}

class CetakInfoProduk {
	public function cetak ( Produk $produk ) {
		$str = "{$produk->judul} | {$produk->getlabel()} (Rp. {$produk->harga})";
		return $str;
	}
}


$produk1 = new Produk("Laskar Pelangi", "Andrea Hirata", "Media Kita", 50000);

$produk2 = new Produk("Belajar WEB Pemula", "Abdul Kadir", "Andi", 250000);



echo "Novel : " . $produk1->getlabel();
echo "<br>";
echo "Buku : " . $produk2->getlabel();

echo "<br><br>";

$infoproduk1 = new CetakInfoProduk();
echo "Novel : " . $infoproduk1->cetak($produk1);


