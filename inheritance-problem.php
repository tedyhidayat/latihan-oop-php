<?php


class Produk {
	public $judul,
			$penulis,
			$penerbit,
			$harga,
			$jmlhalaman,
			$waktumain,
			$tipe;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlhalaman = 0, $waktumain = 0, $tipe ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
		$this->jmlhalaman = $jmlhalaman;
		$this->waktumain = $waktumain;
		$this->tipe = $tipe;
	}

	public function getlabel() {
		return "$this->penulis, $this->penerbit";
	}

	public function getinfolengkap() {
		// Novel : Laskar Pelangi | Andrea Hirata, Media Kita (Rp. 50000) - 100 Halaman.
		// Novel : Mortal Kombat | Ubisoft, Sony Game (Rp. 50000) ~ 50 Jam.
		$str = "{$this->tipe} : {$this->judul} | {$this->getlabel()} (Rp. {$this->harga})";
		if ($this->tipe == "Novel") {
			$str .= " - {$this->jmlhalaman} Halaman.";
		} else if ($this->tipe == "Game") {
			$str .= " ~ {$this->waktumain} Jam.";
		}

		return $str;
	}
}

class CetakInfoProduk {
	public function cetak ( Produk $produk ) {
		$str = "{$produk->judul} | {$produk->getlabel()} (Rp. {$produk->harga})";
		return $str;
	}
}


$produk1 = new Produk("Laskar Pelangi", "Andrea Hirata", "Media Kita", 50000, 100, 0, "Novel");

$produk2 = new Produk("Mortal Kombat", "Ubisoft", "Sony Game", 250000, 0, 30, "Game");



echo $produk1->getinfolengkap();
echo "<br>";
echo $produk2->getinfolengkap();


