<?php

interface Infoproduk{
    public function getinfoproduk();
}

abstract class Produk {
	protected $judul,
			$penulis,
			$penerbit,
			$harga,
			$diskon = 0;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0 ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function setjudul( $judul ) {
		if ( !is_string($judul) ) {
			throw new Exception("Judul Harus berupa STRING");
		}
		$this->judul = $judul;
	}

	public function getjudul() {
		return $this->judul;
	}

	public function setpenulis( $penulis ) {
		if ( !is_string($penulis) ) {
			throw new Exception("penulis Harus berupa STRING");
		}
		$this->penulis = $penulis;
	}

	public function getpenulis() {
		return $this->penulis;
	}

	public function setpenerbit( $penerbit ) {
		if ( !is_string($penerbit) ) {
			throw new Exception("penerbit Harus berupa STRING");
		}
		$this->penerbit = $penerbit;
	}

	public function getpenerbit() {
		return $this->penerbit;
	}

	public function setharga( $harga ) {
		if ( !is_numeric($harga) ) {
			throw new Exception("harga Harus berupa Number");
		}
		$this->harga = $harga;
	}

	public function getharga() {
		return $this->harga - ($this->harga * $this->diskon / 100);
	}

	public function setdiskon( $diskon ) {
		$this->diskon = $diskon;
	}

	public function getdiskon() {
		return $this->diskon;
	}

	public function getlabel() {
		return "$this->penulis, $this->penerbit";
	}

	abstract public function getinfo();

}

class Novel extends Produk implements Infoproduk {
	public $jmlhalaman;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlhalaman = 0 ) {
		parent::__construct( $judul, $penulis, $penerbit, $harga );
		$this->jmlhalaman = $jmlhalaman;
	}

	public function getinfoproduk() {
		$str = "Novel : " . $this->getinfo() . " - {$this->jmlhalaman} Halaman.";
		return $str;
	}

	public function getinfo() {
		$str = "{$this->judul} | {$this->getlabel()} (Rp. {$this->harga})";

		return $str;
	}
}

class Game extends Produk implements Infoproduk {
	public $waktumain;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $waktumain = 0 ) {
		parent::__construct( $judul, $penulis, $penerbit, $harga );
		$this->waktumain = $waktumain;
	}

	public function getinfoproduk() {
		$str = "Game : " . $this->getinfo() . " ~ {$this->waktumain} Jam.";
		return $str;
	}

	public function getinfo() {
		$str = "{$this->judul} | {$this->getlabel()} (Rp. {$this->harga})";

		return $str;
	}
}

class CetakInfoProduk {
	public $daftarproduk = [];

	public function tambahproduk( Produk $produk ) {
		$this->daftarproduk[] = $produk;
	}

	public function cetak () {

		$str = "<h2> DAFTAR PRODUK : </h2>";

		foreach ($this->daftarproduk as $p) {
			$str .= "- {$p->getinfoproduk()} <br>";
		}

		return $str;
	}
}


$produk1 = new Novel("Laskar Pelangi", "Andrea Hirata", "Media Kita", 50000, 100);

$produk2 = new Game("Mortal Kombat", "Ubisoft", "Sony Game", 250000, 30);




$cetakproduk = new CetakInfoProduk;
$cetakproduk->tambahproduk( $produk1 );
$cetakproduk->tambahproduk( $produk2 );

echo $cetakproduk->cetak();

