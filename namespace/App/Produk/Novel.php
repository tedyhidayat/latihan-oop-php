<?php

class Novel extends Produk implements Infoproduk {
	public $jmlhalaman;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlhalaman = 0 ) {
		parent::__construct( $judul, $penulis, $penerbit, $harga );
		$this->jmlhalaman = $jmlhalaman;
	}

	public function getinfoproduk() {
		$str = "Novel : " . $this->getinfo() . " - {$this->jmlhalaman} Halaman.";
		return $str;
	}

	public function getinfo() {
		$str = "{$this->judul} | {$this->getlabel()} (Rp. {$this->harga})";

		return $str;
	}
}