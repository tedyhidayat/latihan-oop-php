<?php


class Produk{
	public $judul = "judul",
			$penulis = "penulis",
			$penerbit = "penerbit",
			$harga = 0;

	public function getlabel(){
		return "$this->judul, $this->penulis";
	}
}

// $produk1 = new Produk();
// $produk1->judul = "Naruto";

// var_dump($produk1);

// $produk2 = new Produk();
// $produk2->judul = "Point Blank";
// $produk2->tambahproperty = "menabahakan property bukan dari dalam class";

// var_dump($produk2);

$produk3 = new Produk();
$produk3->judul = "Laskar Pelangi";
$produk3->penulis = "Andrea Hirata";
$produk3->penerbit = "Media Kita";
$produk3->harga = 75000;

$produk4 = new Produk();
$produk4->judul = "From Zero to Hero";
$produk4->penulis = "Abdul Kadir";
$produk4->penerbit = "Andi";
$produk4->harga = 10000;

echo "Novel : " . $produk3->getlabel();
echo "<br>";
echo "Buku : " . $produk4->getlabel();
