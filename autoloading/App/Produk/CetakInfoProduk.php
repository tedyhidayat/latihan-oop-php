<?php


class CetakInfoProduk {
	public $daftarproduk = [];

	public function tambahproduk( Produk $produk ) {
		$this->daftarproduk[] = $produk;
	}

	public function cetak () {

		$str = "<h2> DAFTAR PRODUK : </h2>";

		foreach ($this->daftarproduk as $p) {
			$str .= "- {$p->getinfoproduk()} <br>";
		}

		return $str;
	}
}