<?php


abstract class Produk {
	protected $judul,
			$penulis,
			$penerbit,
			$harga,
			$diskon = 0;

	public function __construct( $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0 ) {
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function setjudul( $judul ) {
		if ( !is_string($judul) ) {
			throw new Exception("Judul Harus berupa STRING");
		}
		$this->judul = $judul;
	}

	public function getjudul() {
		return $this->judul;
	}

	public function setpenulis( $penulis ) {
		if ( !is_string($penulis) ) {
			throw new Exception("penulis Harus berupa STRING");
		}
		$this->penulis = $penulis;
	}

	public function getpenulis() {
		return $this->penulis;
	}

	public function setpenerbit( $penerbit ) {
		if ( !is_string($penerbit) ) {
			throw new Exception("penerbit Harus berupa STRING");
		}
		$this->penerbit = $penerbit;
	}

	public function getpenerbit() {
		return $this->penerbit;
	}

	public function setharga( $harga ) {
		if ( !is_numeric($harga) ) {
			throw new Exception("harga Harus berupa Number");
		}
		$this->harga = $harga;
	}

	public function getharga() {
		return $this->harga - ($this->harga * $this->diskon / 100);
	}

	public function setdiskon( $diskon ) {
		$this->diskon = $diskon;
	}

	public function getdiskon() {
		return $this->diskon;
	}

	public function getlabel() {
		return "$this->penulis, $this->penerbit";
	}

	abstract public function getinfo();

}